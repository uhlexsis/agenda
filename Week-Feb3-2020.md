# 🗓Feburary 3-7, 2020

*  Working milestone 12.8 and 12.9

# 💬Meetings

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  Plan UX Design sync
*  Plan concepting hangout
*  Holly : Alexis sync
*  Mike: Alexis 1:1
*  Plan stage planning
*  Milestones & releases sync
*  Juan : Alexis design sync
*  UX showcase
*  Mike: Alexis 1:1 year sync
*  Keanon : Alexis design time
*  Fiscal year kickoff


# 🎨Product design tasks

* [ ]  Create an issue around better a better experience for inline editing/saving within the sidebar and beyond 
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* [ ]  Sync with Rayana about Releases, Tags, and Versions 
* [x]  General: Submit expenses
* [ ]  General: Validate Contribute trip
* [ ] Feedback on at least 5 GitLab Design issues
* [ ] How might we best get quick internal feedback on a regular basis?
* [x] Growth plan mural

# 👬Plan stage tasks

* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Ideate on timebox designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  Update the UX Planning, Plan sheet
* [ ]  [Review and create issues around the feedback issue for `NOT` filtering](https://gitlab.com/gitlab-org/gitlab/issues/196129)
* [ ]  [Review and incorporate feedback from `Problem/Solution Validation: How Teams Organize and Track Sprints and Releases`](https://gitlab.com/gitlab-org/ux-research/issues/306)
* [ ]  [Review and incorporate feedback from `Project and Portfolio Management - UX Research Calls`](https://gitlab.com/gitlab-org/ux-research/issues/498)
* [ ]  [Collaborate on `How might the Plan stage team improve focus and cross-functional collaboration on UX-related priorities?`](https://gitlab.com/gitlab-org/gitlab/issues/197993)
* [ ]  [Review and create designs for `Add "Expand All" Button to epic tree view`](https://gitlab.com/gitlab-org/gitlab/issues/197485#note_275484468)
* [x]  [Find out what is going on with the `NOT` MR `Using != filters are slow`](https://gitlab.com/gitlab-org/gitlab/issues/198324#note_275090742)
    * [ ]  Ping John and figure out what happened with the backend review here
* [ ]  Create a UX score card for epics (add to next agenda)
* [ ]  [Review overall designs for `Show milestones in roadmap` ](https://gitlab.com/gitlab-org/gitlab/issues/6802)    
* [ ]  Review [`Issue/Epic: Add ability to edit health status in sidebar`](https://gitlab.com/gitlab-org/gitlab/issues/36427)
* [ ]  Review [`Single Level Group Epics for GitLab Premium`](https://gitlab.com/gitlab-org/gitlab/issues/37081)
* [ ]  [Review feedback in `Return meaningful error when adding one child epic at time and it fails`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/22688#note_281222186)
* [ ]  [Ideate and provide designs for `Only allow valid start and end dates for epics`](https://gitlab.com/gitlab-org/gitlab/issues/10846)
* [ ]  [Update Plan research planning sheet](https://docs.google.com/spreadsheets/d/1YmH_u5FLj03umu1jz7GB2BdbYK8vppFMNGUKPegL_7U/edit#gid=0)
* [ ]  Create research plan for Health status
* [ ]  Create research plan for Epic progress
* [ ]  Schedule time with internal users

# 🌎OKR work
*  [FY21-Q1 UX Quality Product OKR: Increase the value of category maturity ratings by validating them with users => 0%]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)

# ✨Things to explore
*  Coding classes
*  Figma design system

# 🧠Retro and feelings this week
*  🤧Still feeling sick, but better!
