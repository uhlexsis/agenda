# 🗓 September 8-11, 2020

Working milestone 13.5


# 💬Meetings

- Group conversations
- Company calls
- UX weekly
- UX group convo
- Plan concepting hangout
- Keanon : Alexis design time
- Plan stage planning
- Mike and Alexis 1:1
- Andy : Alexis design sync
- Plan UX design sync



# 🎨Product design tasks

- [ ] Review 2 Foundations or cross-product design issues
- [ ] Finish merging button migrations
- [ ] Work with Pedro on Epic issuables

# 👬Plan stage tasks

### 👟️ Priority

- [ ] Create component for `Improve loading state of Boards with swimlanes applied` https://gitlab.com/gitlab-org/gitlab/-/issues/238125
- [x] Design ideation and validation for `Allow users to access their epic boards` https://gitlab.com/gitlab-org/gitlab/-/issues/231391
- [x] `Evaluate responsive tabs` https://gitlab.com/gitlab-org/gitlab-design/-/issues/1352
- [x] Prioritization of `Epic/Program Boards` https://gitlab.com/groups/gitlab-org/-/epics/2864 
- [x] Dovetail issue creation for Portfolio Management metadata
- [ ] Dovetail synthesis for Swimlanes
- [x] Validate `Add issues` bulk experience
- [ ] Create follow up issue for tree https://gitlab.com/gitlab-org/gitlab/-/issues/217862
- [ ] Validate `Refine labeling in the Add epics and issues experience` https://gitlab.com/gitlab-org/gitlab/-/issues/218314


### ⛳️ ️Stretch

- [ ] Complete journey for board
- [ ] Improvement issues for boards 
- [x] Schedule time with at least 2 EMs to better understand boards use cases 
- [x] Schedule time with at least 2 PMs to better understand boards use cases 
- [ ] [Quick ideation and lofi work for cut lines](https://gitlab.com/gitlab-org/gitlab/-/issues/227848) 

### 🙀 Unplanned


# 🌎OKR work

- Dropdown migrations


# 🌵Growth goals
- [ ] Create 15 improvement issues across stages
- [ ] Find a PM course
- [x] Design review weekly
- [x] Follow up with the recruiting team

# 🧠Retro and feelings this week
