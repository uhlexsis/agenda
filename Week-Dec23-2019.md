# 🗓December 23-27, 2019

* I am supposed to be OOO for Christmas this week. :(

# 💬Meetings


# 🎨Product design tasks

* [ ]  Is it known: buttons have no space between them.

# 👬Plan stage tasks


* [x] [Review the `not filtering` MR again ](https://gitlab.com/gitlab-org/gitlab/merge_requests/19011)
* [ ] [Approve `not filtering` MR when changes are made ](https://gitlab.com/gitlab-org/gitlab/merge_requests/19011)
* [x] [ Create a UX debt issue for the next iteration of `not filtering`](https://gitlab.com/gitlab-org/gitlab/issues/121712)
* [ ]  [Work on the flow for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [ ] [Review and work through WIP limits MVC MR](https://gitlab.com/gitlab-org/gitlab/merge_requests/21962#note_264138465)
     * Still blocked due to staging permissions, I pinged Scott.
* [ ]  Create a research issue for Milestone Types 
* [ ]  Create a research issue for WIP limit improvements and needs

# 🌎OKR work


# ✨Things to explore


# 🧠Retro and feelings this week
* I should be OOO this week but a lot of MRs came through late last week.
