# 🗓April 26-30, 2021

* Supposed to be focusing ~100% on the Issues project.

# 💬Meetings
* Dev:Manage/Plan/Ecosystem UX Design Review
* Skip-Level: Christie and UX Dev
* Anne / Alexis ☕️
* UX Weekly Call
* Plan:Product Planning Weekly
* Plan Stage Planning (Fortnightly)
* UX Showcase 
* Plan Office Hours
* Convergence session
* UX Research Office Hours
* UX hangout
* Any AMA or social calls (probably can not attend)
* Alexis & Mike Growth chat (deleted?)

# 🎨Product design tasks
- [ ] Finish mapping out Certify OKR work
- [ ] [Document UT process for research](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1579)
- [ ] To-do based on requests: Refine stale issues to determine relevance, ie https://gitlab.com/gitlab-org/gitlab/-/issues/119024#note_559341332
- [ ] Refine research documentation: [Task: Create a survey to better understand metadata priority](https://gitlab.com/gitlab-org/ux-research/-/issues/1270)
- [ ] Update OOO template
- [ ] Feedback & direction request: [Add a Setup & Testing section to the default MR template](https://gitlab.com/gitlab-org/gitlab/-/issues/324083)
- [ ] Feedback & direction request: [Update the Screenshots section of the default MR template](https://gitlab.com/gitlab-org/gitlab/-/issues/324356)


# 👬Plan stage tasks

###### Blockers
- [ ] Update GDK (🛑blocker)
- [ ] Find out why GitPod isn't working (🛑blocker)

###### High priority
- [ ] "High" priority: Refine UX plan for [Issue Refactoring Design and Engineering Plan](https://gitlab.com/groups/gitlab-org/-/epics/5843)
- [ ] "High" priority: Complete research for Convergence [Issue Refactoring Design and Engineering Plan](https://gitlab.com/groups/gitlab-org/-/epics/5843)

###### Was previously defined as high priority before Issue project required full capacity
- [ ] "High" priority: MR: [Review epic MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/59725#note_557827348)
- [ ] "High" priority: MR: [Review epic boards MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/59446)
- [ ] "High" priority: MR: [Review epic MR](
- [ ] "High" priority: Release post collaboration: [Product Planning Release Post: Reorder epics on a board (MVC 2)](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79899)
- [ ] Design artifact or guidance request: [Determine impact of downgrading tier for epics and multi-level epics](https://gitlab.com/gitlab-org/gitlab/-/issues/328003)
- [ ] Design artifact or guidance request: [Determine impact of downgrading tier for health status multi-level epics](https://gitlab.com/gitlab-org/gitlab/-/issues/328007)
- [ ] Design artifact or guidance request: [Determine impact of downgrading tier for roadmaps](https://gitlab.com/gitlab-org/gitlab/-/issues/328006)
- [ ] Design artifact or guidance request: [Enable custom date picking on the roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/324598)
- [ ] Design artifact or guidance request: [Enable mapping Epics as "Related To" other Epics](https://gitlab.com/gitlab-org/gitlab/-/issues/202431)


###### Requests for designs
- [ ] Design artifact or guidance request: [Refine start & due date widget](https://gitlab.com/gitlab-org/gitlab/-/issues/231401)
- [ ] Design artifact or guidance request: [Add existing epic to an epic board](https://gitlab.com/gitlab-org/gitlab/-/issues/231367)
- [ ] Design artifact or guidance request: [Investigate card styling for graphs in the contributor page](https://gitlab.com/gitlab-org/gitlab/-/issues/301250)
- [ ] Design artifact or guidance request: [Create list object from the board view](https://gitlab.com/gitlab-org/gitlab/-/issues/324261)
- [ ] ⭐️ Design artifact or guidance request: [Filter Roadmap view by set dates instead of infinite horizontal scrolling](https://gitlab.com/gitlab-org/gitlab/-/issues/204994/)
- [ ] Design artifact or guidance request: [Figma specs for expanded swimlanes dropdown- milestone](https://gitlab.com/gitlab-org/gitlab/-/issues/267198#note_536653241)
- [ ] Design artifact or guidance request: [Add filtering by issue type for issues](https://gitlab.com/gitlab-org/gitlab/-/issues/323780)
- [ ] Design artifact or guidance request: [Filter issues by issue_type in issue boards](https://gitlab.com/gitlab-org/gitlab/-/issues/268152)
- [ ] Design artifact or guidance request: [Spike - Linking Requirements and Test Cases](https://gitlab.com/gitlab-org/gitlab/-/issues/297270)
- [ ] Design artifact or guidance request: [Add model restrictions for Requirement issue type](https://gitlab.com/gitlab-org/gitlab/-/issues/323778)
- [ ] Design artifact or guidance request: [Allow one issue to be assigned to multiple epics](https://gitlab.com/gitlab-org/gitlab/-/issues/7503)
- [ ] Design artifact or guidance request: [Service Desk: Annotate external authorship of comments if needed](https://gitlab.com/gitlab-org/gitlab/-/issues/226995)
- [ ] Design artifact or guidance request: [Allow Service Desk participants an opt-out link that removes them from the participants list](https://gitlab.com/gitlab-org/gitlab/-/issues/299261)


###### Requests for collaboration or feedback

- [ ] Feedback & direction request: [Browser extention for issues](https://gitlab.com/gitlab-org/gitlab/-/issues/18925)
- [ ] Feedback & direction request: [Extend guidelines on labels](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/866)
- [ ] Feedback & direction request: [Recently used or viewed epics](https://gitlab.com/gitlab-org/gitlab/-/issues/328331)
- [ ] Feedback & direction request: [Follow up on tooltips or delay interations on hover](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1589)
- [ ] Feedback & direction request: [Button variants for epics that aren't consistent with the recent issue updates](https://gitlab.com/gitlab-org/gitlab/-/issues/267545)
- [ ] Feedback & direction request: [✨Duplicate or move "Create Board" button to be more visible](https://gitlab.com/gitlab-org/gitlab/-/issues/323979)
- [ ] Feedback & direction request: [Downgrade Impacts](https://gitlab.com/gitlab-com/Product/-/issues/1531)
- [ ] Feedback & direction request: [Fix inconsistent drag and drop message in Markdown and Designs](https://gitlab.com/gitlab-org/gitlab/-/issues/283891)
- [ ] Feedback & direction request: [Moving issue to board list of different type should not remove it from previous list](https://gitlab.com/gitlab-org/gitlab/-/issues/325778)
- [ ] Feedback & direction request: vision: [Allow users to gain more context about an epic within the epic board](https://gitlab.com/gitlab-org/gitlab/-/issues/231401)
- [ ] Feedback & direction request: [In Progress Todos Competitive Analysis](https://gitlab.com/gitlab-org/gitlab/-/issues/324678)
- [ ] Feedback & direction request: [Promoted issues should better indicate that they were promoted by the author, not necessarily created by the author](https://gitlab.com/gitlab-org/gitlab/-/issues/325709)
- [ ] Feedback & direction request: debt priority: [UI & UX polish: Adding a list](https://gitlab.com/groups/gitlab-org/-/epics/5637)
- [ ] Feedback & direction request: [Explore better ways to surface "Settings" within the left nav](https://gitlab.com/gitlab-org/gitlab/-/issues/326414)
- [ ] Feedback & direction request: [Display sidebar after user creates a new issue in boards](https://gitlab.com/gitlab-org/gitlab/-/issues/323446)
- [ ] Feedback & direction request: [Epics can have Assignees](https://gitlab.com/groups/gitlab-org/-/epics/4231)
- [ ] Feedback & direction request: [Mobile polish: Make Create list experience more accessible for small screens](https://gitlab.com/gitlab-org/gitlab/-/issues/324721)
- [ ] Feedback & direction request: [Epic Boards: Allow filtering of epics via filter bar](https://gitlab.com/gitlab-org/gitlab/-/issues/322686)
- [ ] Feedback & direction request: [Extensible widgets - related and linked items](https://gitlab.com/gitlab-org/gitlab/-/issues/229731)


# 🌎OKR work


# ✨Things to explore or personal learning
- [ ] Update Growth mural
- [ ] Take a linkedin learning course

# 🧠Retro and feelings this week

