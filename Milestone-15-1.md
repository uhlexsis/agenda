## Milestone 14.10 📆 March 18- April 17

- 🌞 5/27 F & F day
- 🎡 5/30 Public holiday
- 🌴 6/06-6/10 OOO (See OOO issue)

#### 💪 Milestone weight

- Stage target: Up to 15
- Actual: To be filled after milestone

#### 🖇 Stage tasks

###### Themes

- UX OKRs
- Internal notes
- Hierarchy/child linking “widget”
- Stretch: Roadmap dependencies

| Title | Weight | UX notes |Implementation notes|
| ------ | ------ | ------ | ------ |
|Portfolio Mangement JTBD validation|5|UX OKR|Complete by end of 15.2|
|Ensure remaining S1 issue is design complete|2|UX OKR|15.1 design complete|
|Improve Default Project Selection When Creating New Issue from Epic Widget|1|UX OKR|15.1 design complete|
|Adding a scoped label does not always update on a board list|1| UX OKR|15.1 design complete|
|Improve the segmented control button group component|1|Internal notes is dependent|15.1 design complete|
|Improve the toggle component|1|Internal notes is dependent|15.1 design complete|
|Create Internal note icon|1|Internal notes|15.1 design complete|
|Creating a child within the child widget|3|This is a new flow dependent on Work Items|Implementation planned 15.1|
|Add existing child to child widget|1|The scope of work here would not include improvements to the current flow if to be kept minimal|Implementation planned 15.1|
|Remove child from child widget|1||Implementation planned 15.1|
|View & edit child within expanded view|3|||
|View & edit child attributes inline within child widget|5|Research contextual editing of child items & impact and implementation cross-stage, ensure Foundations/Pajamas involvement||
|Child widget adding cta or entry point|1||Implementation planned 15.1|
|Child widget empty state|1|||
|Document Internal notes research synthesis|2|||
|Allow users to add internal notes to a discussion|3||Implementation planned 15.1|
|Allow users reply to an internal note|Support||Implementation planned 15.1|
|Change current button labeling to “Reply” to better indicate to users that they are replying to a thread|Support||Implementation planned 15.1|
|Provide education and affordance to users that a reply is internal|1|||Implementation planned 15.1|
|Move the current “Attach an image or file” option to the comment/internal note header|Support||Implementation planned 15.1|
|Epic color widget|2|||
|Service desk & email footers|.5 |||
|Explore roadmap dependencies|Unknown|||

#### :fox: Org & General tasks

- Interview Product Designers
- Interview Product Managers
- Support with MR ideation
