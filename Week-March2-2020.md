# 🗓March 2-6, 2020

*  Working milestone 12.9 and 12.10


# 💬Meetings

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  UX showcase
*  Plan concepting hangout
*  Timebox affinity map
*  Keanon : Alexis design time
*  Plan stage planning
*  Milestones & releases sync
*  UX group conversation
*  Dev group convo
*  Florie and Alexis roadmaps sync
*  Mile and Alexis 1:1
*  Plan UX design sync


# 🎨Product design tasks

* [x]  [Review research action items for Group, Subgroup, and project icons](https://gitlab.com/gitlab-org/ux-research/issues/569)
* [ ]  [Leave feedback on `Feedback on new format for the retrospective of retrospectives meeting`](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6769)
* [ ]  Epic scorecard
* [ ]  General: do expenses
* [ ]  General: Validate Contribute trip stuff is wrapped up
* [ ] Feedback on at least 5 GitLab Design issues
* [x]  Complete prep work for Continuous Interviewing course
* [x]  Complete practice session for Continuous Interviewing course
* [ ]  Read Figma pilot documentation

# 👬Plan stage tasks

* [x]  [Add feedback to `WIP: Define the process for Category Maturity Scorecards`](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/41210#note_298379512)
* [x]  [Review and give feedback on designs in `Understanding the "Health Status" of my work` ](https://gitlab.com/gitlab-org/gitlab/issues/36427)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [x]  [Work with Florie in `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Scrappy research on start and due dates in `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Scrappy research on designs with Product and Engineering teams for `Make identifiers on epic trees clickable`](https://gitlab.com/gitlab-org/gitlab/issues/197491)
* [ ]  [Design and validate for `Toggle Children Epics and Issues to Confidential`](https://gitlab.com/gitlab-org/gitlab/issues/197341)
* [ ]  [Review for discussion`Epics can have issues assigned from different Group`](https://gitlab.com/gitlab-org/gitlab/issues/205155)
* [ ]  [Review the idea of `Epic templates` for discussion](https://gitlab.com/groups/gitlab-org/-/epics/2237)
* [ ]  [Exploration and mappping for `Set Strategic Initiatives and Goals for my Business` and collaborate on canvas](https://gitlab.com/gitlab-org/gitlab/issues/36775)
* [x]  [Review `Add milestones to roadmap`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/22748)
* [x]  Review [`WIP: Show weight and progress information in Roadmap epic timeline bars`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/18957)
* [x]  Review [`Use colon to tokenize input in filtered search`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/26072)
* [x]  [Finalize MVC designs for `Filter issue list by sub-epic`](https://gitlab.com/gitlab-org/gitlab/issues/9029#note_298469534)
* [x]  [Review forms Pajamas work `Updated form label font weight`](https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1140#note_298463882)
* [x]  [Create and track issue for nesting/child icon `Child epic icon to denote nesting in lists and trees`](https://gitlab.com/gitlab-org/gitlab-design/issues/999#note_298470118)
* [ ]  [Review and add plans to `Roadmaps - UX Research`](https://gitlab.com/gitlab-org/ux-research/issues/354)
* [x]  [Create and review insights in `Improving the accessibility of roadmaps`](https://gitlab.com/gitlab-org/gitlab/issues/208854)
* [x]  [Create an issues for scoped label discrepancies - `Text decoration in scoped labels isn't uniform`](https://gitlab.com/gitlab-org/gitlab/issues/208853)
* [x]  [Create an issue for discussion around input methods `Discussion- supporting various input methods in GitLab`](https://gitlab.com/gitlab-org/gitlab-design/issues/1007)
* [x]  [Review and discuss with Keanon `Inconsistency in issue aggregate filters`](https://gitlab.com/gitlab-org/gitlab/issues/199317)
* [x]  [Design Epic Creation flow for `Allow users to create an Epic within an Epic, consistent with our New Issue pattern`](https://gitlab.com/gitlab-org/gitlab/issues/10966)
* [ ]  [Finalize designs and maybe create another issue for `Weight and progress information in epic`](https://gitlab.com/gitlab-org/gitlab/issues/5163)


# 🌎OKR work
*  [OKR: Increase the value of category maturity ratings by validating them with users](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
*  [OKR: Empower Product Managers and Designers to independently conduct UX Scorecard validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6203)

# 🌵Growth goals

* [ ]  Ideate on Plan research office hours cadence and structure
     * [ ]  Do at least one scrappy test or interview a week with a memeber(s) of Product
* [x]  Buy `Dare to lead` and read 3 chapters
* [x]  Do some research and discovery into the GitLab Forum
* [ ]  Schedule 3 coffee chats with designers 
* [x]  Download `DesignOps Handbook` from the DesignBetter.co library and read 3 chapters
* [ ]  Dig into research issues and how Keanon is conducting these scrappily with users at our next meeting

# ✨Things to explore
*  Figma design system

# 🧠Retro and feelings this week
*  The four day weel made me feel less productive, but i am excited for upcomimg 12.9 work!
