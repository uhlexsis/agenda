# 🗓January 21-24, 2020

*  Working milestone 12.8 and 12.9

# 💬Meetings

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  Plan concepting hangout
*  Holly : Alexis sync
*  Mike: Alexis 1:1
*  Plan stage planning
*  Milestones & releases sync
*  Christie: Dev sync

# 🎨Product design tasks

* [x]  [Review MR for `Revert groups overview font/spacing to the old, more space-efficient layout` ](https://gitlab.com/gitlab-org/gitlab/merge_requests/21584)
* [ ]  [Review results from Group, Subgroup, and project icons surveys](https://gitlab.com/gitlab-org/ux-research/issues/569)
* [ ]  Create an issue around better a better experience for inline editing/saving within the sidebar and beyond 
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* [ ]  Sync with Rayana about Releases, Tags, and Versions 
* [ ]  General: Submit expenses
* [ ]  General: Validate Contribute trip
* [ ] Feedback on at least 5 GitLab Design issues
* [ ] How might we best get quick internal feedback on a regular basis?

# 👬Plan stage tasks

* [x]  [Work on testing prototype for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Ideate on timebox designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [x]  Create a research issue for WIP limit improvements and needs (For Holly to create now-add to agenda)
* [x]  Update the UX Planning, Plan sheet
* [ ]  [Review and create issues around the feedback issue for `NOT` filtering](https://gitlab.com/gitlab-org/gitlab/issues/196129)
* [ ]  [Review and incorporate feedback from `Problem/Solution Validation: How Teams Organize and Track Sprints and Releases`](https://gitlab.com/gitlab-org/ux-research/issues/306)
* [ ]  [Review and incorporate feedback from `Project and Portfolio Management - UX Research Calls`](https://gitlab.com/gitlab-org/ux-research/issues/498)
* [x]  [Review and create any needed issues for `Add remove limit button to wip limit`](https://gitlab.com/gitlab-org/gitlab/merge_requests/22552)
* [ ]  [Collaborate on `How might the Plan stage team improve focus and cross-functional collaboration on UX-related priorities?`](https://gitlab.com/gitlab-org/gitlab/issues/197993)
* [ ]  [Review and create designs for `Add "Expand All" Button to epic tree view`](https://gitlab.com/gitlab-org/gitlab/issues/197485#note_275484468)
* [x]  [Find out what is going on with the `NOT` MR `Using != filters are slow`](https://gitlab.com/gitlab-org/gitlab/issues/198324#note_275090742)
    * [ ]  Ping John and figure out what happened with the backend review here
* [x]  [Empty states for `Show milestones in roadmap` ](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [x]  [Review labels popover in `Update labels in gitlab-ui to use interactive popover`](https://gitlab.com/gitlab-org/gitlab-ui/issues/466#note_274662717)
* [x]  [Review MR `WIP: Expand epics in roadmap to view hierarchy`](https://gitlab.com/gitlab-org/gitlab/merge_requests/23600)
* [x]  [Create scrappy research around `Allow users to set work in progress limits by weight` and share results with Gabe and Scott](https://gitlab.com/gitlab-org/gitlab/issues/119208)
* [x]  [Review `Improve docs experience for Experience Baseline JTBD: Understand dependencies as they relate to a larger scope`](https://gitlab.com/gitlab-org/gitlab/issues/37739)
* [ ]  Create a UX score card for epics (add to next agenda)
* [x]  Create and hand off WIP limit issues
    * [x]  [Allow users to set work in progress limits by weight](https://gitlab.com/gitlab-org/gitlab/issues/119208 )    
    * [x]  [Truncated label styling in boards and more contextual tooltips](https://gitlab.com/gitlab-org/gitlab/issues/199371 )   
    * [x]  [Improving spacing within WIP limits component](https://gitlab.com/gitlab-org/gitlab/issues/199441 )  
    * [x]  [Move remove list option to the settings list](https://gitlab.com/gitlab-org/gitlab/issues/199444 ) 
    * [x]  [Missing focus outline for input](https://gitlab.com/gitlab-org/gitlab-ui/issues/539)   
    * [x]  [Form Validation for WIP Limits](https://gitlab.com/gitlab-org/gitlab/issues/199182   )
    * [x]  [Drawer styling when it contains sidebar content ](https://gitlab.com/gitlab-org/gitlab-ui/issues/577)  
    * [x]  [Expected interaction when clicking on a label within List settings](https://gitlab.com/gitlab-org/gitlab/issues/199446 )
* [x]  [Review and collaborate on Stage Level (Plan) Customer Product Partnership Pilot](https://gitlab.com/gitlab-com/Product/issues/749)
* [ ]  [Review overall designs for `Show milestones in roadmap` ](https://gitlab.com/gitlab-org/gitlab/issues/6802)    
* [ ]  Review [`Issue/Epic: Add ability to edit health status in sidebar`](https://gitlab.com/gitlab-org/gitlab/issues/36427)
* [ ]  Review [`Single Level Group Epics for GitLab Premium`](https://gitlab.com/gitlab-org/gitlab/issues/37081)

# 🌎OKR work
*  [FY21-Q1 UX Quality Product OKR: Increase the value of category maturity ratings by validating them with users => 0%]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)

# ✨Things to explore
*  Coding classes
*  Figma design system

# 🧠Retro and feelings this week
*  🤧I was sick this week so I feel I wasn't able to complete as much. 
