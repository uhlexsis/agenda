# 🗓 Nov 23-27, 2020

Working milestone 13.7 & 13.8


# 💬Meetings

- Group conversations
- Company calls
- UX weekly
- Plan concepting hangout
- Mike and Alexis 1:1
- Libor : Alexis design sync
- Anne : Alexis



# 🎨Product design tasks

- [ ] Review 2 Foundations or cross-product design issues
- [ ] Finish merging tab migrations
- [ ] Finish documentation and Figma tweaks for alerts https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2180


# 👬Plan stage tasks

### 👟️ Priority

- [ ] Support Jeremy in organization of Product Planning https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/68837#note_453712122
- [ ] Usertesting.com pilot and documentation
    - [ ] Screener
    - [ ] Feedback doc for Adam
- [ ] Validation for https://gitlab.com/gitlab-org/gitlab/-/issues/284610
    - [ ] Screener in usertesting
- [ ] Demo environment set up
    - [ ] Schedule coffee chat with Jeff M
- [ ] Refine Epic Boards work
- [ ] OOO issue completed for Mike, Christen, and Holly

### ⛳️ ️Stretch

- [ ] Create follow up issue for tree https://gitlab.com/gitlab-org/gitlab/-/issues/217862
- [ ] Validate `Refine labeling in the Add epics and issues experience` https://gitlab.com/gitlab-org/gitlab/-/issues/218314
- [ ] Improvement issues for boards 
- [ ] Update into Figma component for `Improve loading state of Boards with swimlanes applied` https://gitlab.com/gitlab-org/gitlab/-/issues/238125
- [ ] Actionable issue creation for https://dovetailapp.com/projects/426c18af-e27f-4863-b58c-d5d0b51da9fb/insights and https://dovetailapp.com/projects/ae3ce191-5b94-4b83-87d2-bc0137eebf0c/data/b/dea8e84a-1f9d-40cf-be82-9c1a3de004b4

### 🙀 Unplanned


# 🌎OKR work

- [] Weekly dogfooding of Iterations
- Pajamas migrations


# 🌵Growth goals
- [ ] Create 15 improvement issues across stages
- [ ] Design feedback guidelines MR
- [ ] Watch >4 Chorus videos across stages

# 🧠Retro and feelings this week
- 🦃 Stressin' because the world. Happy because the holidays and awesome coworkers and family. 
