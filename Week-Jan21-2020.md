# 🗓January 21-24, 2020

*  Working milestone 12.8 and 12.9

# 💬Meetings
Very light meetings! Yay!

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  Plan concepting hangout
*  Juan : Alexis design sync
*  Mike: Marcel: Alexis
*  Katherine: Alexis research priorities sync
*  Holly : Alexis sync
*  Mike: Alexis 1:1
*  Plan stage planning
*  Milestones & releases sync
*  External user feedback session: timeboxes and insights

# 🎨Product design tasks

* [ ]  [Review MR for `Revert groups overview font/spacing to the old, more space-efficient layout` ](https://gitlab.com/gitlab-org/gitlab/merge_requests/21584)
* [ ]  [eview results from Group, Subgroup, and project icons surveys](https://gitlab.com/gitlab-org/ux-research/issues/569)
* [ ]  Create an issue around better a better experience for inline editing/saving within the sidebar and beyond 
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* [ ]  Sync with Rayana about Releases, Tags, and Versions 
* [ ]  General: Submit expenses
* [ ]  General: Validate Contribute trip
* [ ]  Task Nick with a [UX scorecard](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/)
* [ ]  Create icon issue for timebox types (on hold until we do more research)
* [ ] Feedback on at least 5 GitLab Design issues

# 👬Plan stage tasks

* [ ]  [Work on testing prototype for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Ideate on timebox designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  Create a research issue for WIP limit improvements and needs
* [x]  Update the UX Planning, Plan sheet
* [ ]  [Review and create issues around the feedback issue for `NOT` filtering](https://gitlab.com/gitlab-org/gitlab/issues/196129)
* [ ]  [Review and incorporate feedback from `Problem/Solution Validation: How Teams Organize and Track Sprints and Releases`](https://gitlab.com/gitlab-org/ux-research/issues/306)
* [ ]  [Review and incorporate feedback from `Project and Portfolio Management - UX Research Calls`](https://gitlab.com/gitlab-org/ux-research/issues/498)
* [x]  Final review of MR for [`Frontend: Use custom user name for service desk emails`](https://gitlab.com/gitlab-org/gitlab/merge_requests/22478)
   * This will have to move to 12.8 since Kushal is out
* [ ]  [Review and create any needed issues for `Add remove limit button to wip limit`](https://gitlab.com/gitlab-org/gitlab/merge_requests/22552)
* [ ]  [Collaborate on `How might the Plan stage team improve focus and cross-functional collaboration on UX-related priorities?`](https://gitlab.com/gitlab-org/gitlab/issues/197993)
* [ ]  [Review and create designs for `Add "Expand All" Button to epic tree view`](https://gitlab.com/gitlab-org/gitlab/issues/197485#note_275484468)
* [x]  External user interview on Timeboxes (#1)
* [x]  [Review `Fixes error when assigning an existing assignee`](https://gitlab.com/gitlab-org/gitlab/merge_requests/23416#note_275311983)
* [ ]  [Find out what is going on with the `NOT` MR `Using != filters are slow`](https://gitlab.com/gitlab-org/gitlab/issues/198324#note_275090742)
* [ ]  [Empty states for `Show milestones in roadmap` ](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Review labels popover in `Update labels in gitlab-ui to use interactive popover`](https://gitlab.com/gitlab-org/gitlab-ui/issues/466#note_274662717)
* [x]  [Review MR `WIP: Expand epics in roadmap to view hierarchy`](https://gitlab.com/gitlab-org/gitlab/merge_requests/23600)
* [ ]  [Create scrappy research around `Allow users to set work in progress limits by weight` and share results with Gabe and Scott](https://gitlab.com/gitlab-org/gitlab/issues/119208)
* [x]  [Review `Switch dropdown operators to lowercase`](https://gitlab.com/gitlab-org/gitlab/merge_requests/23692#note_276601649)
* [ ]  [Review `Improve docs experience for Experience Baseline JTBD: Understand dependencies as they relate to a larger scope`](https://gitlab.com/gitlab-org/gitlab/issues/37739)


# 🌎OKR work
*  [FY21-Q1 UX Quality Product OKR: Increase the value of category maturity ratings by validating them with users => 0%]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)

# ✨Things to explore
*  Coding classes
*  Figma design system

# 🧠Retro and feelings this week
*  
