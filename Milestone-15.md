## Milestone 15.0 📆 April 18- May 17

- 🎡 4/15&18 Easter
- 🦊New Plan designer starts on the 19th
- 💖 4/16 - 10 days Working from London to spend time with partner

#### :lifter: Milestone effort


###### Total issue weight:
   - `43`
###### Areas worked on:
   - Issues
   - Epics
   - Merge Requests
   - Iteration/cadence
   - Boards
   - Markdown
   - Object: comment



#### 🖇 Workflow tasks

| Title | Weight | Notes|
| ------ | ------ | ------ |
| Iternal notes granular design ideation and decomposition| 8 |[+Planned+]|
| Validate internal note solution | 5 |[+Planned+]|
| Update issueable badges  | 2 |[-Unplanned-]|
| Improve UX and error messaging around test case title length | 2 |[-Unplanned-]|
| Update issuable confidentiality UI & status text | 2 |[+Planned+]|
| Fixed spacing for the loading icon in time tracker| .5 |[-Unplanned-]|
| Allow `markdown_continue_lists` flag for all markdown fields| 1 |[-Unplanned-]|
| Boards - Polish top buttons and add new column on mobile   | 2 |[-Unplanned-]|
| Rename confidential notes to internal notes | .5 |[+Planned+]|
| Design for selected MR hypotheses | 8 |[+Planned+]|
|Child widget MVC|8|[+Planned+]|
|Design support for schedule iterations on cadence start day|2|[-Unplanned-]|
|Iterate on display & wrapping of status badges within headers to improve usability on small screens|2|[-Unplanned-]|



#### :fox: Org & General 
- Onboard new designer
- Cross-stage feedback efforts
- Interview designers
