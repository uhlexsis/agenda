# 🗓July 13-17, 2020

Working milestones 13.2 and 13.2


# 💬Meetings

- Group conversations
- Company calls
- Portfolio and Certify board review
- UX weekly
- Plan concepting hangout
- Keanon : Alexis design time
- Plan stage planning
- Mike and Alexis 1:1
- Andy : Alexis design sync
- Plan UX design sync
- Design Management usability test
- JTBD sync with Daniel
- Sync with Recruiting team about boards
- Alexis : Jarek D&D documentation sync
- Alexis : Austin sync


# 🎨Product design tasks

- [ ] Review 2 Foundations or cross-product design issues
- [ ] [Wrap up drag and drop design documentation](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/1941) 


# 👬Plan stage tasks

### 👟️ Priority


- [ ] Schedule time with at least 2 EMs to better understand boards use cases (aim for next week)
- [ ] Schedule time with at least 2 PMs to better understand boards use cases (aim for next week)
- [ ] [Create testing enviroment for metadata research](https://gitlab.com/gitlab-org/ux-research/-/issues/901)
- [ ] [Break down program board work from a UX perspective](https://gitlab.com/groups/gitlab-org/-/epics/2864)
- [ ] [Quick ideation and lofi work for cut lines](https://gitlab.com/gitlab-org/gitlab/-/issues/227848) 


### ⛳️ ️Stretch

- [ ] Journey for board
- [ ] Improvement issues for boards

# 🌎OKR work

- Copy tweaks and UI polish



# 🌵Growth goals
- [ ] Research how others communicate and break down final designs
- [ ] Schedule time with Kyle
- [ ] Identify my WIP and impliment it in my column


# 🧠Retro and feelings this week