# 🗓December 16-20, 2019

*  Working milestone 12.6 and 12.7
*  Onboarding a new Plan designer! 🎉
*  I will be in SF this week, let me know if you would like to cowork. 

# 💬Meetings

*Light on meetings this week!*

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  Jeremy: Alexis OKR sync
*  Dev group conversation
*  UX group conversation
*  UX weekly
*  Alexis : Holly sync
*  Alexis : Nick onboarding sync
*  Alexis : Pedro Issues + MRs sync
*  Requirements MVC design sync
*  1-1
*  Portfolio management sync
*  Plan concepting hangout

# 🎨Product design tasks

* [x]  [Maintain MR for `Revert groups overview font/spacing to the old, more space-efficient layout` ](https://gitlab.com/gitlab-org/gitlab/merge_requests/21584)
* [x]  [Creating mocks and testing plan for Project, Group, and Subgroup icons.](https://gitlab.com/gitlab-org/gitlab-design/issues/697) Holly asked to be involved.
* [x]  [ Create a research issue for Group, Subgroup, and project icons and ping Holly as she asked to be involved ](https://gitlab.com/gitlab-org/ux-research/issues/569)
* [ ]  Create an issue around better a better experience for inline editing/saving within the sidebar and beyond 
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* [ ]  Review Amelia's solution for [`Backend: Add ability to embed stack trace in gitlab issue`](https://gitlab.com/gitlab-org/gitlab/issues/36544)
* Creating and maintaining an onboarding issue for our new designer Nick
* [x]  Add slides to Dev group conversation
* [x]  Add slides to UX group conversation
* [x]  Schedule and set an agenda for Nick
* [x]  Do expenses
* [ ] Feedback on at least 5 GitLab Design issues

# 👬Plan stage tasks

* [x]  [Review and give feedback on designs in `Understanding the "Health Status" of my work` ](https://gitlab.com/gitlab-org/gitlab/issues/36427)
* [x]  [Review MR for `weight and progress information in roadmap bars`](https://gitlab.com/gitlab-org/gitlab/merge_requests/18957)
* [x]  [Create UX Debt issue or epic for `weight and progress information in roadmap bars`](https://gitlab.com/gitlab-org/gitlab/issues/119024)
* [ ]  [Work on the flow for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [x]  [Upload designs and specs for `Visually Differentiate Blocked Issues`](https://gitlab.com/gitlab-org/gitlab/issues/34723)
* [x]  [Work on designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
    * `Blocked` : needs some PM feedback and collaboration to work through an edge case.
* [x]  [Work on designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [x]  [Move `Show milestones in roadmap` out of the `design` workflow](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [x]  [ Work on design for weight limits for WIP limits MVC slight scope changes](https://gitlab.com/gitlab-org/gitlab/issues/11403)
* [x] [ Review and work through WIP limits MVC MR](https://gitlab.com/gitlab-org/gitlab/merge_requests/21962)
* [x] [ Create an issue around the issue/weight limit scope change.](https://gitlab.com/gitlab-org/gitlab/issues/119208#)
* [x] [ Quick cleanup on `Configure label to be removed when issue is closed.`](https://gitlab.com/gitlab-org/gitlab/issues/17461)
* [ ]  Create a research issue for Milestone Types 
* [ ]  Create a research issue for WIP limit improvements and needs
* [x]  Update the UX Planning, Plan sheet

# 🌎OKR work
*  [ Beautify UI OKR]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/5573)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)

# ✨Things to explore
*  Coding classes
*  Figma design system

# 🧠Retro and feelings this week
*  I will be out for 2 weeks starting next week, so I am feeling like I better get as much complete as possible! 🚀
