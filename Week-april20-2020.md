# 🗓March 20-24, 2020

*  Working milestone 13 and 13.1


# 💬Meetings

*  Group conversations
*  Company calls
*  Portfolio and Certify board review
*  UX weekly
*  Plan concepting hangout
*  Keanon : Alexis design time
*  Plan stage planning
*  Mike and Alexis 1:1
*  Andy : Alexis design sync
*  Plan UX design sync
*  Remote Contribute
*  Roadmaps scaling and debt sync


# 🎨Product design tasks

* [ ]  Finish Roadmaps category maturity research issue 
* [ ]  Interview 5 internal users
* [ ]  Feedback on at least 5 GitLab Design issues
* [ ]  Read Figma pilot documentation
* [ ]  Figma polish
* [ ]  Move tree to Figma

# 👬Plan stage tasks

###### 👟️ Priority

* [ ]  Schedule time with at least 3 EMs to better understand Epics within boards use cases
    * [ ]  EM 1
    * [ ]  EM 2
    * [ ]  EM 3
* [ ]  [Rough designs for validation in `Filter Roadmap view by set dates instead of infinite horizontal scrolling`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Validation issue for `Assign Parent Epic to current Epic from Sidebar`
* [x]  [Review and add plans to `Roadmaps - UX Research`](https://gitlab.com/gitlab-org/ux-research/issues/354)
* [ ]  [Action on insights in `Improving the accessibility of roadmaps`](https://gitlab.com/gitlab-org/gitlab/issues/208854)
* [ ]  Create research and validation issue for health status in epics
* [x]  [Follow up on Add "Expand All" Button to epic tree view to make sure the loading indicator is in a good state](https://gitlab.com/gitlab-org/gitlab/-/issues/197485)

###### ⛳️ ️Stretch
* [ ]  [Review `Make weights cross functionally usable`](https://gitlab.com/gitlab-org/gitlab/-/issues/197880)
* [ ]  [Review discussion around input methods `Discussion- supporting various input methods in GitLab`](https://gitlab.com/gitlab-org/gitlab-design/issues/1007)
* [ ]  [Review `Inconsistency in issue aggregate filters`](https://gitlab.com/gitlab-org/gitlab/issues/199317)
* [ ]  Iterate on milestone designs
* [ ]  [Review the idea of `Epic templates` for discussion](https://gitlab.com/groups/gitlab-org/-/epics/2237)
* [ ]  [Exploration and mapping for `Set Strategic Initiatives and Goals for my Business` and collaborate on canvas](https://gitlab.com/gitlab-org/gitlab/issues/36775)
* [ ]  [Design and validate for `Toggle Children Epics and Issues to Confidential`](https://gitlab.com/gitlab-org/gitlab/issues/197341)
* [ ]  [Review for discussion`Epics can have issues assigned from different Group`](https://gitlab.com/gitlab-org/gitlab/issues/205155)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Scrappy research on start and due dates in `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)

# 🌎OKR work
*  [OKR: Increase the value of category maturity ratings by validating them with users](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
*  [OKR: Empower Product Managers and Designers to independently conduct UX Scorecard validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6203)

# 🌵Growth goals

* [ ]  Ideate on Plan research office hours cadence and structure
     * [ ]  Do at least one scrappy test or interview a week with a memeber(s) of Product
* [ ]  Read `Dare to lead` - 3 chapters
* [ ]  Do some research and discovery into the GitLab Forum
* [ ]  Schedule 3 coffee chats with designers 
* [ ]  Dig into research issues and how Keanon is conducting these scrappily with users at our next meeting

# ✨Things to explore
*  Figma design system

# 🧠Retro and feelings this week
