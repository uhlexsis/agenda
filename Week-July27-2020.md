
# 🗓July 27-31, 2020

Working milestones 13.3


# 💬Meetings

- User interviews- metadata
Group conversations
- Company calls
- Portfolio and Certify board review
- UX weekly
- Plan concepting hangout
- Keanon : Alexis design time
- Nadia: Holly: Alexis
- Nick: Holly: Alexis
- Plan stage planning
- Mike and Alexis 1:1
- Valerie: Alexis
- Andy : Alexis design sync
- Plan UX design sync



# 🎨Product design tasks

- [ ] Review 2 Foundations or cross-product design issues
- [x] [Wrap up drag and drop design documentation](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/1941) 


# 👬Plan stage tasks

### 👟️ Priority


- [ ] Journey for board
- [ ] Improvement issues for boards
- [x] [Create testing enviroment for metadata research](https://gitlab.com/gitlab-org/ux-research/-/issues/901)
- [x] [Break down program board work from a UX perspective](https://gitlab.com/groups/gitlab-org/-/epics/2864)
- [ ] [Quick ideation and lofi work for cut lines](https://gitlab.com/gitlab-org/gitlab/-/issues/227848) 
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36421 
- [ ] Epics can have Assignees https://gitlab.com/gitlab-org/gitlab/-/issues/227839
- [ ] Increase the Epic Nesting from 5 to X https://gitlab.com/gitlab-org/gitlab/-/issues/232665

### ⛳️ ️Stretch
- [ ] Schedule time with at least 2 EMs to better understand boards use cases (aim for next week)
- [ ] Schedule time with at least 2 PMs to better understand boards use cases (aim for next week)
- [ ] Filter component docs

# 🌎OKR work

- Copy tweaks and UI polish



# 🌵Growth goals
- [ ] Research how others communicate and break down final designs
- [ ] Schedule time with Kyle
- [ ] Identify my WIP and impliment it in my column


# 🧠Retro and feelings this week
