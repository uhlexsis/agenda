## Milestone 14.10 📆 March 18- April 17

- 🌞 3/25 F & F day
- 💖 3/27- 4/14 Working from Austin to spend time with family
- 🌞 4/11 F & F day
- 🎡 4/15&18 Easter

#### :lifter: Milestone effort


###### Total issue weight:
   - `38.5`
###### Areas worked on:
   - Service Desk
   - Issues
   - Epics
   - Merge Requests
   - Iteration/cadence
   - Boards
   - Milestones
   - Roadmaps
   - Markdown
   - Object: comment
   - Component: dropdown


#### 🖇 Workflow tasks

| Title | Weight | Notes|
| ------ | ------ | ------ |
| “Confidential comments” discovery and ideation | 13 |[+Planned+]|
| Iteration cadences manual scheduling deprecation  | 5 |[-Unplanned-]|
| Tasks design | 3 |[-Unplanned-]|
| MR Hypothesis workshop | 5 |[+Planned+]|
| Better indicate & reenforce visibility or confidentiality status | 2 |[+Planned+]|
| Milestones UX Improvements - Empty state for milestones needs UX help | 2 |[-Unplanned-]|
| GitLab's Service Desk allows spoofing issue creator via reply-to email header  | 3 |[-Unplanned-]|
| Follow up on epic tree and epic roadmap app polish | 2 |[+Planned+]|
| Issue boards: add ability to filter by iteration cadence | 2 |[-Unplanned-]|
| Remove gray background from milestone page to unbox GitLab | .5 |[-Unplanned-]|
| Issue description with front matter breaks task lists  | .5 |[-Unplanned-]|
| Replace bootstrap dropdown in app/views/shared/labels/_sort_dropdown.html.hamlion.so/14-10-87df1f0543824f69a50fc19 | .5 |[-Unplanned-]|


#### :fox: Org & General 
- Wrapping up Product Manager interviews
