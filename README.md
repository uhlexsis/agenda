# About me

Hey! 👋 My name is Alexis Ginsberg, I am a Senior Product Designer at GitLab focusing on the Plan stage.

# Purpose of this project

This project is to view what I am working on this week at GitLab.

# How to get in touch

* Current location: Chicago, IL in the USA
* Timezone: CST
* I check both email and Slack. Urgent requests should be through Slack.


 