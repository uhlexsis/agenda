# 🗓January 13-17, 2020

*  Working milestone 12.7 and 12.8
*  Onboarding a new Plan designer! 🎉
*  We have a new manager starting this week, woohoo!

# 💬Meetings
Light meetings this week! 🙌

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  Plan concepting hangout
*  Juan : Alexis design sync
*  Nick : Alexis sync
*  Holly : Alexis sync
*  Project management opportunity canvas sync
*  Figma working group meeting
*  Plan stage planning
*  Milestones & releases sync
*  Monthly release kickoff

# 🎨Product design tasks

* [x]  [Maintain MR for `Revert groups overview font/spacing to the old, more space-efficient layout` ](https://gitlab.com/gitlab-org/gitlab/merge_requests/21584)
* [x]  [ Create a test for Group, Subgroup, and project icons](https://gitlab.com/gitlab-org/ux-research/issues/569)
* [ ]  Create an issue around better a better experience for inline editing/saving within the sidebar and beyond 
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* Review and close onboarding issue for our new designer Nick
* [x]  Make sure Nick and Holly are invited to Plan retros
* Sync with Rayana about Releases, Tags, and Versions (do this next week outside of the team call)
* [ ]  General: Submit expenses
* [ ]  General: Validate Contribute trip
* [ ]  Task Nick with a [UX scorecard](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/)
* [ ]  Create icon issue for timebox types (on hold until we do more research)
* [ ] Feedback on at least 5 GitLab Design issues

# 👬Plan stage tasks

* [x] Quick design ideas and guidance on [`Add comment summary to the link's tooltip when the link points to a comment`](https://gitlab.com/gitlab-org/gitlab/issues/29663)
* [ ]  [Review and give feedback on designs in `Understanding the "Health Status" of my work` ](https://gitlab.com/gitlab-org/gitlab/issues/36427)
* [x]  Review and give feedback on designs in [`Show new activity in an issue since my last visit`](https://gitlab.com/gitlab-org/gitlab/issues/30661)
* [x] Review and give feedback on designs in [`Issue and MR activity in feed order (newest first)` ](https://gitlab.com/gitlab-org/gitlab/issues/14588)
* [x]  [Work on the flow for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Ideate on timebox designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [x] [ Review and work plan for timeboxes 
* [ ]  Create a research issue for WIP limit improvements and needs
* [x]  Update the UX Planning, Plan sheet
* [x]  Create new and improved planning sheet for the team
* [ ]  [Review and create issues around the feedback issue for `NOT` filtering](https://gitlab.com/gitlab-org/gitlab/issues/196129)
* [x]  Review and comment on the project management opportunity canvas
* [ ]  [Review and incorporate feedback from `Problem/Solution Validation: How Teams Organize and Track Sprints and Releases`](https://gitlab.com/gitlab-org/ux-research/issues/306)
* [ ]  [Review and incorporate feedback from `Project and Portfolio Management - UX Research Calls`](https://gitlab.com/gitlab-org/ux-research/issues/498)
* [x] [Review MR for WIP limit tooltips](https://gitlab.com/gitlab-org/gitlab/merge_requests/22820)
* [x]  Review MR for [`Frontend: Use custom user name for service desk emails`](https://gitlab.com/gitlab-org/gitlab/merge_requests/22478)
   * This will have to move to 12.8 since Kushal is out
* [x]  Review [`Add comment summary to the link's tooltip when the link points to a comment`](https://gitlab.com/gitlab-org/gitlab/issues/29663)
* [x]  Create issues based on [`Add comment summary to the link's tooltip when the link points to a comment`](https://gitlab.com/gitlab-org/gitlab/issues/29663)
* [x]  [Create an issue around drawer styling improvements in gitlab-ui `Drawer styling when it contains sidebar content`](https://gitlab.com/gitlab-org/gitlab-ui/issues/577#note_271454688)
* [x]  [Create an issue around adding tooltips to the drawer compononent in gitlab-ui `Add a tooltip the close icon in the drawer component`](https://gitlab.com/gitlab-org/gitlab-ui/issues/578)
* [x] [Do some research on quick fixes for text filtering in `Improve text search while filtering`](https://gitlab.com/gitlab-org/gitlab/issues/196126)
    * More context: https://gitlab.slack.com/archives/CETG54GQ0/p1579020816089600
    * Added to the research initiative for 12.8
* [ ]  [Review and create any needed issues for `Add remove limit button to wip limit`](https://gitlab.com/gitlab-org/gitlab/merge_requests/22552)
* [x]  Create survey for timebox names (merge into larger issue now that we have more time for research)
* [x]  Create research issue for Timebox concept validation
* [x]  Create research issue for `NOT` filtering usability and concept
* [x]  [Address WIP UX debt in `The Board List Issuable sidebar and board list settings overlap when both are opened`](https://gitlab.com/gitlab-org/gitlab/issues/121712)
* [x]  [UX review of `Added delay to drag event in manual ordering`](https://gitlab.com/gitlab-org/gitlab/merge_requests/23252#note_273345326)
# 🌎OKR work
*  [ Beautify UI OKR]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/5573)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)

# ✨Things to explore
*  Coding classes
*  Figma design system

# 🧠Retro and feelings this week
*  Lots of work getting done! 🚀
