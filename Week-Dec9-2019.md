# 🗓December 9-13, 2019

*  Working milestone 12.6 and 12.7
*  Onboarding a new Plan designer! 🎉
*  Will be out on Friday 13 for a funeral.
*  I will be in SF for a week next week, let me know if you would like to cowork. 

# 💬Meetings
*  Group conversations
*  Company calls
*  Plan issue board review
*  Sidebar sync
*  UX weekly
*  Alexis : Juan UX sync
*  Plan social call
*  Alexis : Holly sync
*  Alexis : Nick onboarding sync
*  Taurie : Alexis sync
*  1-1
*  UX Stage Manager Meeting, Plan 
*  Simon : Alexis sync
*  Portfolio management sync
*  Scott: Alexis sync
*  Plan concepting hangout

# 🎨Product design tasks

* [ ]  [Create an MR for `Revert groups overview font/spacing to the old, more space-efficient layout` ](https://gitlab.com/gitlab-org/gitlab/issues/29987)
* [ ]  [Creating mocks and testing plan for Project, Group, and Subgroup icons.](https://gitlab.com/gitlab-org/gitlab-design/issues/697) Holly asked to be involved.
* [ ]  [ Create a research issue for Group, Subgroup, and project icons ](https://gitlab.com/gitlab-org/gitlab-design/issues/697)
* [ ]  Create an issue around better a better experience for inline editing/saving within the sidebar and beyond 
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* [ ]  Review Amelia's solution for [`Backend: Add ability to embed stack trace in gitlab issue`](https://gitlab.com/gitlab-org/gitlab/issues/36544)
* [x]  Create an agenda for Nick's onboarding
* Creating and maintaining an onboarding issue for our new designer Nick
* Feedback on GitLab Design issues

# 👬Plan stage tasks

* [ ]  [Review MR for `weight and progress information in roadmap bars`](https://gitlab.com/gitlab-org/gitlab/merge_requests/18957)
* [x]  Provide new designs due to contraints in `weight and progress information in roadmap bars`
* [x]  [Review MR for `Use buffered rendering for Roadmap epics`](https://gitlab.com/gitlab-org/gitlab/merge_requests/19875)
* [x]  [ Review MR for `Remove dynamic size computations in Roadmap to speed up rendering`](https://gitlab.com/gitlab-org/gitlab/merge_requests/21172)
* [x ]  [Review MR for `promoting confidential issues may expose confidential information`](https://gitlab.com/gitlab-org/gitlab/merge_requests/21158)
* [ ]  [Work on the flow for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [ ]  [Work on designs for `Visually Differentiate Blocked Issues`](https://gitlab.com/gitlab-org/gitlab/issues/34723)
* [ ]  [ Work on design for weight limits for WIP limits MVC slight scope changes](https://gitlab.com/gitlab-org/gitlab/issues/11403)
* [x]  [Work on designs for `Expand epics in roadmap to view hierarchy`](https://gitlab.com/gitlab-org/gitlab/issues/7077)
* [ ]  [Work on designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Work on designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Review prior design work on `Blocking issues MVC: (add support for issue dependencies)`](https://gitlab.com/gitlab-org/gitlab/issues/2035)

# 🌎OKR work
*  [ Beautify UI OKR]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/5573)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)

# ✨Things to explore
*  Coding classes
*  Figma design system

# 🧠Retro and feelings this week
* I had a hard time completing all my work this week due to meetings peppered throughout the day M-F and being out on Friday for the funeral. :()

