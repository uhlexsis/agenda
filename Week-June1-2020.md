# 🗓 June 1-5, 2020

*  Working milestone 13.1 and 13.2


# 💬Meetings

*  Group conversations
*  UX Foundations office hours
*  Portfolio and Certify board review
*  UX weekly
*  Plan concepting hangout
*  Plan board inception  (4 hour block)
*  Keanon : Alexis design time
*  Plan stage planning
*  Mike and Alexis pair design
*  Andy : Alexis design sync
*  Plan UX design sync
*  Iteration office hours
*  Think Big-Issue types
*  Chat on Strategic Planning Vision- Craig
*  Hackathon check-in
*  Alexis: Marcin
*  Amplitude Product Summit


# 🎨Product design tasks

* [ ]  [Work on drag and drop documentation](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/1941)
* [ ]  Update Portfolio Management JTBD Research
* [ ]  Feedback on at least 5 GitLab Design issues
* [ ]  Organize Figma
* [ ]  Ask Jeremy about capacity for swimlanes

# 👬Plan stage tasks

###### 👟️ Priority
* [ ] [Allow for filtering of health status in board views](https://gitlab.com/gitlab-org/gitlab/-/issues/212473)
* [ ] [Show Health Status on Issue cards in a Board](Show Health Status on Issue cards in a Board)
* [ ] [Add Issue health status as an option for board lists](https://gitlab.com/gitlab-org/gitlab/-/issues/213694)
* [ ] [Enable mapping Epics as "Related To" other Epics](https://gitlab.com/gitlab-org/gitlab/-/issues/202431)
* [ ]  [Exploration and mapping for `Set Strategic Initiatives and Goals for my Business` and collaborate on canvas](https://gitlab.com/gitlab-org/gitlab/issues/36775)
* [ ]  [Validation issue for `Roadmap progress bars to say "X out of Y recorded weight completed" to represent that all issues may not be weighted`
* [ ]  [Review issue for nesting/child icon `Child epic icon to denote nesting in lists and trees`](https://gitlab.com/gitlab-org/gitlab-design/issues/999#note_298470118)
* [ ]  Create research and validation issue for health status in epics
* [ ]  Open research issue for epic tree
* [ ]  Open research issue for issue metadata
* 
###### ⛳️ ️Stretch
* [ ] [Track epic completion across milestones](https://gitlab.com/gitlab-org/gitlab/-/issues/7795)
* [ ]  [Rough designs for validation in `Filter Roadmap view by set dates instead of infinite horizontal scrolling`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Validation issue for `Assign Parent Epic to current Epic from Sidebar`
* [ ]  [Validation issue for `Provide the Epic summary tooltip for sub-epics within an Epic Tree`
* [ ]  [Review `Make weights cross functionally usable`](https://gitlab.com/gitlab-org/gitlab/-/issues/197880)
* [ ]  [Review discussion around input methods `Discussion- supporting various input methods in GitLab`](https://gitlab.com/gitlab-org/gitlab-design/issues/1007)
* [ ]  [Review `Inconsistency in issue aggregate filters`](https://gitlab.com/gitlab-org/gitlab/issues/199317)
* [ ]  [Review the idea of `Epic templates` for discussion](https://gitlab.com/groups/gitlab-org/-/epics/2237)
* [ ]  [Exploration and mapping for `Set Strategic Initiatives and Goals for my Business` and collaborate on canvas](https://gitlab.com/gitlab-org/gitlab/issues/36775)
* [ ]  [Design and validate for `Toggle Children Epics and Issues to Confidential`](https://gitlab.com/gitlab-org/gitlab/issues/197341)
* [ ]  [Review for discussion`Epics can have issues assigned from different Group`](https://gitlab.com/gitlab-org/gitlab/issues/205155)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Scrappy research on start and due dates in `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Review and add plans to `Roadmaps - UX Research`](https://gitlab.com/gitlab-org/ux-research/issues/354)
* [ ]  [Action on insights in `Improving the accessibility of roadmaps`](https://gitlab.com/gitlab-org/gitlab/issues/208854)


# 🌵Growth goals

* [ ]  Ideate on Plan research office hours cadence and structure
     * [ ]  Do at least one scrappy test or interview a week with a memeber(s) of Product
* [x]  Buy `Dare to lead` and read 3 chapters
* [x]  Do some research and discovery into the GitLab Forum
* [ ]  Schedule 3 coffee chats with designers 
* [x]  Download `DesignOps Handbook` from the DesignBetter.co library and read 3 chapters
* [ ]  Dig into research issues and how Keanon is conducting these scrappily with users at our next meeting

# ✨Things to explore
*  Figma design system

# 🧠Retro and feelings this week
*  