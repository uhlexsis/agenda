# 🗓 Oct 13-16, 2020

Working milestone 13.6


# 💬Meetings

- Group conversations
- Company calls
- UX weekly
- Plan concepting hangout
- Keanon : Alexis design time
- Plan stage planning
- Mike and Alexis 1:1
- Andy : Alexis design sync
- Mark: Alexis
- UX Showcase



# 🎨Product design tasks

- [ ] Review 2 Foundations or cross-product design issues
    - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/267113
- [ ] Finish merging tab migrations
- [ ] Finish merging tooltip migrations

     - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/228972
     - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/228993
     - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/229011
- [ ] Work with Rayana on Settings

# 👬Plan stage tasks

### 👟️ Priority

- [ ] CMS script and recruiting issues
- [ ] Update into Figma component for `Improve loading state of Boards with swimlanes applied` https://gitlab.com/gitlab-org/gitlab/-/issues/238125
- [ ] Actionable issue creation for https://dovetailapp.com/projects/426c18af-e27f-4863-b58c-d5d0b51da9fb/insights and https://dovetailapp.com/projects/ae3ce191-5b94-4b83-87d2-bc0137eebf0c/data/b/dea8e84a-1f9d-40cf-be82-9c1a3de004b4
- [ ] Board empty state with swimlanes and not
- [ ] Swimlanes expansion https://gitlab.com/groups/gitlab-org/-/epics/328
    - [ ] Make sure these are in iterations
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/44380
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/43681
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/issues/238125#note_428501445

### ⛳️ ️Stretch

- [ ] Create follow up issue for tree https://gitlab.com/gitlab-org/gitlab/-/issues/217862
- [ ] Validate `Refine labeling in the Add epics and issues experience` https://gitlab.com/gitlab-org/gitlab/-/issues/218314
- [ ] Complete journey for board
- [ ] Complete comp analysis for board
- [ ] Improvement issues for boards 

### 🙀 Unplanned
- [ ] Growth upsell in sidebar https://www.figma.com/file/1wY6r6bkfHxCrTRwCL1Dse/Untitled?node-id=22%3A333
- [ ] Review older designs in https://gitlab.com/gitlab-org/gitlab/-/issues/36167#note_425620784
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45141
- [ ] Iterate on https://gitlab.com/gitlab-org/gitlab/-/issues/30249
- [ ] Iteration feedback and issue creation
- [ ] Add list tweaks
- [ ] Loader component
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/44482
# 🌎OKR work

- [x] Weekly dogfooding of Iterations
- Pajamas migrations


# 🌵Growth goals
- [ ] Create 15 improvement issues across stages
    - [ ] Move "dropdown" CTA
- [ ] Design feedback guidelines MR
- [ ] Design chats
- [ ] Watch >4 Chorus videos across stages

# 🧠Retro and feelings this week
