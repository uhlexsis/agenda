# 🗓 August 9-13, 2021

* Supposed to be focusing ~100% on the "Work Items" project.

# 💬Meetings
Current meeting hours: ~6
* Dev:Manage/Plan/Ecosystem UX Design Review
* UX Weekly Call
* Product Planning / work item weekly 
* Plan office hours
* Alexis: Jeremy
* Alexis: Christen
* Alexis: Holly
* Interview: Senior UX Researcher
* Plan Stage Planning (Fortnightly)
* Plan social call (probably can not attend)
* UX hangout (probably can not attend)
* Any AMA or social calls (probably can not attend)


# 🎨Product design tasks
- [x] Help Pedro with MR OKR survey
- [ ] Ideate on frameworks for focus
- [ ] Leave feedback on CE
- [ ] Write in retro
- [ ] Interview candidates
- [ ] Document drawer interactions in Pajamas
- [ ] (Stretch) Reseearch CM & JTBD process
- [ ] (Stretch) Research design weights and capacity documentation
- [ ] (Stretch) Finish mapping Category research
- [ ] (Stretch)[Document UT process for research](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1579)
- [ ] (Stretch) Update OOO template

# 👬Plan stage tasks

###### High priority
- [ ] Create actionable insight issues from JTBD research
- [ ] Create rough proposals for actionable insights
- [ ] [🔬Collect and synthesize prior research- Product Planning design ideation: Project Level Epics](https://gitlab.com/gitlab-org/ux-research/-/issues/1412)
- [ ] Link high-level dependency ideation to issues for discussion
- [ ] Refine scope and plan for MVC 2
- [ ] Review MRs

# 🌎OKR work
- [Scorecard Product Planning with Mike](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12084)
- [Roadmaps JTBD validation research study](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12126)
- [Provide examples of MR reviews](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12127)

# ✨Things to explore or personal learning
- [ ] Update Growth & vision
- [ ] Take a linkedin learning course

# 🧠Retro and feelings this week



