# 🗓December 6-10, 2020

*  Working milestone 12.7 and 12.8
*  Onboarding a new Plan designer! 🎉
*  We have a new manager starting next week, woohoo!

# 💬Meetings

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  UX Plan issue planning
*  Plan concepting hangout
*  UX showcase
*  Interview a Product Designer for Configure
*  UX dev section meeting
*  Jackie : Alexis sync
*  Sid : Alexis sync
*  Kai : Alexis sync
*  Patrick : Alexis sync
*  James : Alexis sync
*  Mike : Alexis release design sync
*  Milestone types design review

# 🎨Product design tasks

* [ ]  [Maintain MR for `Revert groups overview font/spacing to the old, more space-efficient layout` ](https://gitlab.com/gitlab-org/gitlab/merge_requests/21584)
* [ ]  [ Create a test for Group, Subgroup, and project icons and ping Holly as she asked to be involved ](https://gitlab.com/gitlab-org/ux-research/issues/569)
* [ ]  Create an issue around better a better experience for inline editing/saving within the sidebar and beyond 
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* [ ]  Review Amelia's solution for [`Backend: Add ability to embed stack trace in gitlab issue`](https://gitlab.com/gitlab-org/gitlab/issues/36544)
* Review and close onboarding issue for our new designer Nick
* [ ] Feedback on at least 5 GitLab Design issues

# 👬Plan stage tasks

* [x]  Conduct 5 internal user interviews
* [ ]  [Review and give feedback on designs in `Understanding the "Health Status" of my work` ](https://gitlab.com/gitlab-org/gitlab/issues/36427)
* [x]  [Review MR for `weight and progress information in roadmap bars`](https://gitlab.com/gitlab-org/gitlab/merge_requests/18957)
* [x]  [Create UX Debt issue or epic for `weight and progress information in roadmap bars`](https://gitlab.com/gitlab-org/gitlab/issues/119024)
* [ ]  [Work on the flow for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [x]  [Upload designs and specs for `Visually Differentiate Blocked Issues`](https://gitlab.com/gitlab-org/gitlab/issues/34723)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Ideate on timebox designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ] [ Review and work through WIP limits MVC Style MR](https://gitlab.com/gitlab-org/gitlab/merge_requests/22552
* [ ]  Create a research issue for Milestone Types 
* [ ]  Create a research issue for WIP limit improvements and needs
* [x]  Update the UX Planning, Plan sheet
* [x]  [Ideate on post latest concept for milestone types ](https://gitlab.com/gitlab-org/gitlab/issues/35290#note_268955101)
* [x]  [Create and share feedback issue for `NOT` filtering](https://gitlab.com/gitlab-org/gitlab/issues/196129)

# 🌎OKR work
*  [ Beautify UI OKR]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/5573)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)

# ✨Things to explore
*  Coding classes
*  Figma design system

# 🧠Retro and feelings this week
*  Lots of work getting done! 🚀
