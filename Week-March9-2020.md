# 🗓March 2-6, 2020

*  Working milestone 12.10


# 💬Meetings

*  Group conversations
*  Company calls
*  Sid AMA
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  Plan concepting hangout
*  Keanon : Alexis design time
*  Plan stage planning
*  Milestones & releases sync
*  UX group conversation
*  Mike and Alexis 1:1
*  Holly : Alexis sync
*  Plan UX design sync


# 🎨Product design tasks

* [ ]  [Leave feedback on `Feedback on new format for the retrospective of retrospectives meeting`](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6769)
* [ ]  Epic scorecard
* [ ]  General: do expenses
* [ ]  General: Validate Contribute trip stuff is wrapped up
* [ ]  Feedback on at least 5 GitLab Design issues
* [ ]  Read Figma pilot documentation
* [ ] [Have a follow up chat with Keanon on Roadmap category maturity](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/41210) 

# 👬Plan stage tasks

* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [ ]  [Scrappy research on start and due dates in `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Scrappy research on designs with Product and Engineering teams for `Make identifiers on epic trees clickable`](https://gitlab.com/gitlab-org/gitlab/issues/197491)
* [ ]  [Design and validate for `Toggle Children Epics and Issues to Confidential`](https://gitlab.com/gitlab-org/gitlab/issues/197341)
* [ ]  [Review for discussion`Epics can have issues assigned from different Group`](https://gitlab.com/gitlab-org/gitlab/issues/205155)
* [ ]  [Review the idea of `Epic templates` for discussion](https://gitlab.com/groups/gitlab-org/-/epics/2237)
* [ ]  [Exploration and mappping for `Set Strategic Initiatives and Goals for my Business` and collaborate on canvas](https://gitlab.com/gitlab-org/gitlab/issues/36775)
* [ ]  [Review `Add milestones to roadmap`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/22748)
* [ ]  [Document MVC designs for `Filter issue list by sub-epic`](https://gitlab.com/gitlab-org/gitlab/issues/9029#note_298469534)
* [x]  [Review forms Pajamas work Added default input examples to forms.md`](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/1745)
* [ ]  [Review issue for nesting/child icon `Child epic icon to denote nesting in lists and trees`](https://gitlab.com/gitlab-org/gitlab-design/issues/999#note_298470118)
* [ ]  [Review and add plans to `Roadmaps - UX Research`](https://gitlab.com/gitlab-org/ux-research/issues/354)
* [ ]  [Action on insights in `Improving the accessibility of roadmaps`](https://gitlab.com/gitlab-org/gitlab/issues/208854)
* [ ]  [Review discussion around input methods `Discussion- supporting various input methods in GitLab`](https://gitlab.com/gitlab-org/gitlab-design/issues/1007)
* [ ]  [Review `Inconsistency in issue aggregate filters`](https://gitlab.com/gitlab-org/gitlab/issues/199317)
* [ ]  [Design future-scope Epic Creation flow for `Allow users to create an Epic within an Epic, consistent with our New Issue pattern`](https://gitlab.com/gitlab-org/gitlab/issues/10966)
* [ ]  [Finalize designs and maybe create another issue for `Weight and progress information in epic`](https://gitlab.com/gitlab-org/gitlab/issues/5163)
* [x]  [Review WIP: Support async loading & search of projects](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/26661)
* [ ]  Create research and validation issue for health status in epics


# 🌎OKR work
*  [OKR: Increase the value of category maturity ratings by validating them with users](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
*  [OKR: Empower Product Managers and Designers to independently conduct UX Scorecard validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6203)

# 🌵Growth goals

* [ ]  Ideate on Plan research office hours cadence and structure
     * [ ]  Do at least one scrappy test or interview a week with a memeber(s) of Product
* [x]  Buy `Dare to lead` and read 3 chapters
* [x]  Do some research and discovery into the GitLab Forum
* [ ]  Schedule 3 coffee chats with designers 
* [x]  Download `DesignOps Handbook` from the DesignBetter.co library and read 3 chapters
* [ ]  Dig into research issues and how Keanon is conducting these scrappily with users at our next meeting

# ✨Things to explore
*  Figma design system

# 🧠Retro and feelings this week
*  