# 🗓February 10-14, 2020

*  Working milestone 12.8 and 12.9


# 💬Meetings

*  Group conversations
*  Company calls
*  Plan issue board review
*  Portfolio and Certify board review
*  UX weekly
*  Plan concepting hangout
*  Keanon : Alexis design time
*  Plan stage planning
*  Milestones & releases sync
*  UX group conversation
*  Katherine research sync
*  User interview 1 (timeboxes and advanced search)
*  User interview 2 (timeboxes and advanced search)
*  User interview 3 (timeboxes and advanced search)
*  Continuous interviewing course session
*  Interview candidate for Senior Product Designer, CI/CD
*  Alexis: Marcin coffee chat
*  Plan UX design sync
*  Florie : Alexis Milestone sync
*  Mike : Alexis 1-1
*  Donald: Alexis: Keanon Milestones sync


# 🎨Product design tasks

* [ ]  [Review research action items for Group, Subgroup, and project icons](https://gitlab.com/gitlab-org/ux-research/issues/569)
* [ ]  Epic scorecard
* [ ]  UX showcase
* [ ]  Sync with Rayana about Releases, Tags, and Versions (do this next week outside of the team call)
* [ ]  General: do expenses
* [ ]  General: Validate Contribute trip
* [ ]  Create icon issue for `Expand all`
* [ ] Feedback on at least 5 GitLab Design issues
* [x]  Complete prep work for Continuous Interviewing course
* [ ]  Complete practice session for Continuous Interviewing course
* [ ]  Read Figma pilot documentation

# 👬Plan stage tasks

* [ ]  [Conduct and synthesize 5 user interviews for  `External user feedback on Timeboxes` ](https://gitlab.com/gitlab-org/ux-research/issues/623)
* [ ]  [Conduct and synthesize 5 user interviews for  `Advanced search functionality iteration and validation`](https://gitlab.com/gitlab-org/ux-research/issues/617)
* [ ]  [Review and give feedback on designs in `Understanding the "Health Status" of my work` ](https://gitlab.com/gitlab-org/gitlab/issues/36427)
* [x]  [Review the updated requirements for `Enable timeboxes`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [ ]  [Ideate on timebox designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [x]  [Work with Keanon and Donald to unblock Florie in `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [ ]  [Scrappy research on start and due dates in `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [x]  [Review and create issues around the feedback issue for `NOT` filtering](https://gitlab.com/gitlab-org/gitlab/issues/196129)
* [ ]  [Review and incorporate feedback from `Problem/Solution Validation: How Teams Organize and Track Sprints and Releases`](https://gitlab.com/gitlab-org/ux-research/issues/306)
* [x]  [Review and incorporate feedback from `Project and Portfolio Management - UX Research Calls`](https://gitlab.com/gitlab-org/ux-research/issues/498)
* [x]  [Create flow and designs for `Add "Expand All" Button to epic tree view`](https://gitlab.com/gitlab-org/gitlab/issues/197485)
* [x]  [Review MR `Improve Roadmap today indicator rendering logic`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/24669)
* [x]  [Review MR `WIP: Add epic in filtered search`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/22958)
    * Blocked until Holly takes a look as well
* [ ]  [Add UX thoughts on `Make create issue form on epics tree support more than 20 projects`](https://gitlab.com/gitlab-org/gitlab/issues/119320)
* [x]  [Scrappy research to Product and Engineering teams for `Make identifiers on epic trees clickable`](https://gitlab.com/gitlab-org/gitlab/issues/197491)
* [x]  [Add designs for `Make identifiers on epic trees clickable`](https://gitlab.com/gitlab-org/gitlab/issues/197491)
* [ ]  [Design and validate for `Toggle Children Epics and Issues to Confidential`](https://gitlab.com/gitlab-org/gitlab/issues/197341)
* [ ]  [Review for discussion`Epics can have issues assigned from different Group`](https://gitlab.com/gitlab-org/gitlab/issues/205155)
* [x]  [Review the idea of `Epic templates` for discussion](https://gitlab.com/groups/gitlab-org/-/epics/2237)
* [ ]  [Collaborate with FE on `Collapsing and expanding already-loaded epics has no click feedback`]( https://gitlab.com/gitlab-org/gitlab/issues/202008)
* [x]  [Wrap up design work for `Single Level Group Epics for GitLab Premium` and make sure anything needed is in the design tab](https://gitlab.com/gitlab-org/gitlab/issues/37081)
* [x]  [Review `Cannot search for issues with the word "label" without fancy JS doing the wrong thing1](https://gitlab.com/gitlab-org/gitlab/issues/196084)
* [ ]  [Synthesize customer calls in `Customer Research for Strategic Planning`](https://gitlab.com/gitlab-org/gitlab/issues/202671)
* [ ]  [Exploration and mappping for `Set Strategic Initiatives and Goals for my Business` and collaborate on canvas](https://gitlab.com/gitlab-org/gitlab/issues/36775)




# 🌎OKR work
*  [OKR: Increase the value of category maturity ratings by validating them with users](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201)
*  [OKR: Empower Product Managers and Designers to independently conduct UX Scorecard validation](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6203)

# 🌵Growth goals

* [ ]  Ideate on Plan research office hours cadence and structure
     * [x]  Do at least one scrappy test or interview a week with a memeber(s) of Product
* [x]  Buy `Dare to lead` and read 3 chapters
* [x]  Do some research and discovery into the GitLab Forum
* [ ]  Schedule 3 coffee chats with designers 
* [x]  Download `DesignOps Handbook` from the DesignBetter.co library and read 3 chapters
* [ ]  Dig into research issues and how Keanon is conducting these scrappily with users at our next meeting

# ✨Things to explore
*  Figma design system

# 🧠Retro and feelings this week
*  Lots of meetings, a bit hard to focus on Porfolio Management while I wrap up Project Management and UX admin work.
