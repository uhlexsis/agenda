# 🗓 August 3-7, 2020

Working milestone 13.3


# 💬Meetings

- Group conversations
- Company calls
- Portfolio and Certify board review
- Issuables discussion
- UX weekly
- Plan concepting hangout
- Keanon : Alexis design time
- Plan stage planning
- Mike and Alexis 1:1
- Jeremy: Alexis boards sync
- Andy : Alexis design sync
- Plan UX design sync
- Recruiting team boards sync



# 🎨Product design tasks

- [ ] Review 2 Foundations or cross-product design issues
- [x] Create issue for the filter component 

# 👬Plan stage tasks

### 👟️ Priority

- [x] `Show Health Status on Issue cards in a Board` https://gitlab.com/gitlab-org/gitlab/-/issues/216967
- [ ] `Add Issue health status as an option for board lists` https://gitlab.com/gitlab-org/gitlab/-/issues/213694
- [x] `Text in Epic tree not properly lined up` https://gitlab.com/gitlab-org/gitlab/-/issues/201807
- [ ] Create follow up issue for tree https://gitlab.com/gitlab-org/gitlab/-/issues/217862
- [ ] Review `UI feedback when user tries to add already attached issue to epic` https://gitlab.com/gitlab-org/gitlab/-/issues/196836
- [ ] Validate `Refine labeling in the Add epics and issues experience` https://gitlab.com/gitlab-org/gitlab/-/issues/218314
- [ ] Final review of `Epic/Program Boards` https://gitlab.com/groups/gitlab-org/-/epics/2864 
- [ ] Epics can have Assignees https://gitlab.com/gitlab-org/gitlab/-/issues/227839
- [ ] Increase the Epic Nesting from 5 to X https://gitlab.com/gitlab-org/gitlab/-/issues/232665
- [ ] Dovetail synthesis for Portfolio Management metadata

### ⛳️ ️Stretch

- [ ] Complete journey for board
- [ ] Improvement issues for boards 
- [ ] Schedule time with at least 2 EMs to better understand boards use cases 
- [ ] Schedule time with at least 2 PMs to better understand boards use cases 
- [ ] Filter component docs
- [ ] [Quick ideation and lofi work for cut lines](https://gitlab.com/gitlab-org/gitlab/-/issues/227848) 

### 🙀 Unplanned
- [x] `Edit issue title in swimlanes sidebar` https://gitlab.com/gitlab-org/gitlab/-/issues/232745
- [ ] Review copy and MR of `Board refactor - Move fetch lists GraphQL to VueX` https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37905#note_390050690
- [x] `Validate expected interactions on Issue Board issue cards` https://gitlab.com/gitlab-org/gitlab/-/issues/233498
- [ ] Create issue around disabled sidebar element in https://gitlab.com/gitlab-org/gitlab/-/issues/220867#note_391506504

# 🌎OKR work

- Copy tweaks and UI polish


# 🌵Growth goals
- [ ] Create 15 improvement issues across stages
- [x] Collaborate with 2 designers outside of Plan (Pedro and Jeremy this week)
- [ ] Review RM and sidebars with Nick


# 🧠Retro and feelings this week
