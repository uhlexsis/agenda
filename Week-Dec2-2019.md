# 🗓December 2-6, 2019

*  Working milestone 12.6 and 12.7
*  Onboarding a new Plan designer! 🎉

# 💬Meetings
*  Group conversations
*  Company calls
*  Plan issue board review
*  Sidebar sync
*  UX weekly
*  Kushal : Alexis sync
*  1-1
*  UX showcase
*  Plan concepting hangout
*  L&D: Live Learning - Inclusion Training
*  Coung : Donald : Alexis roadmap sync
*  Jeremy : Alexis sync

# 🎨Product design tasks
* [ Beautify UI OKR]( https://gitlab.com/gitlab-com/www-gitlab-com/issues/5573)
*  [Better documentation OKR with Russell](https://gitlab.com/gitlab-org/gitlab-design/issues/692)
* [ Group, Subgroup, and project icons ](https://gitlab.com/gitlab-org/gitlab-design/issues/697)
* [Sidebar documentation and improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/451)
* Creating and maintaining an onboarding issue for our new designer Nick
* Feedback on GitLab Design issues

# 👬Plan stage tasks
*  [Review MR for `weight and progress information in roadmap bars`](https://gitlab.com/gitlab-org/gitlab/merge_requests/18957)
*  Prove new designs due to contraints in `weight and progress information in roadmap bars`
*  [Review MR for `Use buffered rendering for Roadmap epics`](https://gitlab.com/gitlab-org/gitlab/merge_requests/19875)
* [ Review MR for `Remove dynamic size computations in Roadmap to speed up rendering`](https://gitlab.com/gitlab-org/gitlab/merge_requests/21172)
*  [Review MR for `promoting confidential issues may expose confidential information`](https://gitlab.com/gitlab-org/gitlab/merge_requests/21158)
*  [Work on the flow for `Define types of milestones`](https://gitlab.com/gitlab-org/gitlab/issues/35290)
* [ Work on design for weight limits for WIP limits MVC slight scope changes](https://gitlab.com/gitlab-org/gitlab/issues/11403)
* [Work on designs for `Expand epics in roadmap to view hierarchy`](https://gitlab.com/gitlab-org/gitlab/issues/7077)
* [Work on designs for `View fixed vs inherited start and due dates of epics on roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/7076)
* [Work on designs for `Show milestones in roadmap`](https://gitlab.com/gitlab-org/gitlab/issues/6802)
* [Review prior design work on `Blocking issues MVC: (add support for issue dependencies)`](https://gitlab.com/gitlab-org/gitlab/issues/2035)

# 🧠Retro and feelings this week
*  I have been feeling a bit tired and jetlagged due to coming off a 2 week vacation, but happy to be back!
*  My Grandfather passed away last week, so I was at a bit lower capacity dealing with that.
