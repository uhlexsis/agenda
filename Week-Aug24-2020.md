# 🗓 August 24-28, 2020

Working milestone 13.4 and 13.5


# 💬Meetings

- Group conversations
- Company calls
- Portfolio and Certify board review
- UX weekly
- Plan concepting hangout
- Keanon : Alexis design time
- Plan stage planning
- Mike and Alexis 1:1
- Andy : Alexis design sync
- Plan UX design sync



# 🎨Product design tasks

- [ ] Review 2 Foundations or cross-product design issues
- [ ] Finish merging button migrations
- [ ] Work with Pedro on Epic issuables

# 👬Plan stage tasks

### 👟️ Priority

- [ ] `Improve loading state of Boards with swimlanes applied` https://gitlab.com/gitlab-org/gitlab/-/issues/238125
- [ ] `Add Issue health status as an option for board lists` https://gitlab.com/gitlab-org/gitlab/-/issues/213694
- [ ] Create follow up issue for tree https://gitlab.com/gitlab-org/gitlab/-/issues/217862
- [ ] Review `UI feedback when user tries to add already attached issue to epic` https://gitlab.com/gitlab-org/gitlab/-/issues/196836
- [ ] Validate `Refine labeling in the Add epics and issues experience` https://gitlab.com/gitlab-org/gitlab/-/issues/218314
- [ ] Final review of `Epic/Program Boards` https://gitlab.com/groups/gitlab-org/-/epics/2864 
- [ ] Epics can have Assignees https://gitlab.com/gitlab-org/gitlab/-/issues/227839
- [ ] Increase the Epic Nesting from 5 to X https://gitlab.com/gitlab-org/gitlab/-/issues/232665
- [ ] Dovetail synthesis for Portfolio Management metadata

### ⛳️ ️Stretch

- [ ] Complete journey for board
- [ ] Improvement issues for boards 
- [ ] Schedule time with at least 2 EMs to better understand boards use cases 
- [ ] Schedule time with at least 2 PMs to better understand boards use cases 
- [ ] Filter component docs
- [ ] [Quick ideation and lofi work for cut lines](https://gitlab.com/gitlab-org/gitlab/-/issues/227848) 

### 🙀 Unplanned


# 🌎OKR work

- Button migrations


# 🌵Growth goals
- [ ] Create 15 improvement issues across stages
- [ ] Find a PM course

# 🧠Retro and feelings this week
